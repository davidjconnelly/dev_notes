<h1><?= $headline ?></h1>
<?= validation_errors() ?>
<div class="w3-card-4">
    <div class="w3-container primary">
        <h4>Snippet Details</h4>
    </div>
    <form class="w3-container" action="<?= $form_location ?>" method="post">

        <p>
            <label class="w3-text-dark-grey"><b>Snippet Headline</b></label>
            <input type="text" name="snippet_headline" value="<?= $snippet_headline ?>" class="w3-input w3-border w3-sand" placeholder="Enter Snippet Headline">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Category ID</b></label>
            <input type="text" name="category_id" value="<?= $category_id ?>" class="w3-input w3-border w3-sand" placeholder="Enter Category ID">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Date Created</b></label>
            <input type="text" name="date_created" value="<?= $date_created ?>" class="w3-input w3-border w3-sand" placeholder="Enter Date Created">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Code</b></label>
            <input type="text" name="code" value="<?= $code ?>" class="w3-input w3-border w3-sand" placeholder="Enter Code">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Last Updated</b></label>
            <input type="text" name="last_updated" value="<?= $last_updated ?>" class="w3-input w3-border w3-sand" placeholder="Enter Last Updated">
        </p>
        <p>
            <label class="w3-text-dark-grey">Public</label>
            <input name="public" class="w3-check" type="checkbox" value="1"<?php if ($public==1) { echo ' checked="checked"'; } ?>>
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Information</b></label>
            <textarea name="information" class="w3-input w3-border w3-sand" placeholder="Enter Information here..."><?= $information ?></textarea>
        </p>
        <p> 
            <?php 
            $attributes['class'] = 'w3-button w3-white w3-border';
            echo anchor($cancel_url, 'CANCEL', $attributes);
            ?> 
            <button type="submit" name="submit" value="Submit" class="w3-button w3-medium primary"><?= $btn_text ?></button>
        </p>
    </form>
</div>

<script>
$('.datepicker').datepicker();
$('.datetimepicker').datetimepicker({
    separator: ' at '
});
$('.timepicker').timepicker();
</script>