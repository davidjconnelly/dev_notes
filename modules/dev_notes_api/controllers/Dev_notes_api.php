<?php
class Dev_notes_api extends Trongate {

	private $trial_length = 400;

	function test() {
		$data['type'] = 'buy';
		$new_key = $this->_generate_new_api_key($data);
		echo $new_key; die();
	}

	function _generate_new_api_key($data) {
		extract($data);

		if ($type == 'trial') {
			$key = time();
		} else {
			$key = make_rand_str(32);
		}

		$this->module('encryption');
		$new_key = $this->encryption->_encrypt($key);
		return $new_key;
	}

	function _is_api_key_valid($api_key) {
		//returns true or false
		error_reporting(0);
		$this->module('encryption');
	    $decrypted_key = $this->encryption->_decrypt($api_key);

	    if (is_numeric($decrypted_key)) {

	    	$nowtime = time();
	    	$ancient_history = $nowtime - $this->trial_length;
			if ($decrypted_key>$ancient_history) {
				return true;
			} else {
				return false;
			}

	    } else {

	    	//do we have a matching licence key?
	    	$licence_obj = $this->model->get_one_where('licence_key', $decrypted_key, 'licence_keys');

	    	if ($licence_obj == false) {
	    		return false;
	    	} else {
	    		return true;
	    	}

	    }

	}

	function search() {

$data = [];
$this->_interupt($data);


		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");
		http_response_code(200);

		$posted_data = file_get_contents('php://input');
		$data = json_decode($posted_data, true);

		if ($data == false) {
			$data = $_POST;
		} 

		$params['search_phrase'] = '%'.$data['searchPhrase'].'%';

		$sql = 'SELECT
				snippets.*
				FROM
				snippets
				JOIN snippet_categories
				ON snippets.category_id = snippet_categories.id 
				WHERE snippets.snippet_headline LIKE :search_phrase 
				OR snippets.information LIKE :search_phrase 
				OR snippet_categories.category_title LIKE :search_phrase 
				ORDER BY last_updated desc';

		$rows = $this->model->query_bind($sql, $params, 'object');
		echo json_encode($rows);
	}

	function hello_api() {
$data = [];
$this->_interupt($data);

		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");
		http_response_code(200);
		echo 'Record everything.  Memorize nothing.'; die(); //this can be changed
	}

	function display_categories() {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");
		$params['user_id'] = 1;
		$sql = 'select * from snippet_categories where user_id = :user_id order by category_title';
		$rows = $this->model->query_bind($sql, $params, 'object');
		echo json_encode($rows);
	}

	function view_category($category_id) {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		$params['category_id'] = $category_id;

		$sql = 'select * from snippets where category_id = :category_id order by snippet_headline';
		$data['rows'] = $this->model->query_bind($sql, $params, 'object');
		$category = $this->model->get_where($category_id, 'snippet_categories');
		$data['category_title'] = $category->category_title; 
		$data['category_id'] = $category_id;
	
		echo json_encode($data);
	}

	function select_category($category_id) {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		$category = $this->model->get_where($category_id, 'snippet_categories');
		$data['category_title'] = $category->category_title; 
		$data['id'] = $category->id;
	
		echo json_encode($data);		
	}

	function view_record($update_id) {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		$record = $this->model->get_where($update_id, 'snippets');

		if ($record == false) {
			http_response_code(400); //bad request
			echo 'Not found'; die();
		}

		$fourth_bit = $this->url->segment(4);
		if ($fourth_bit == 'true') {
			//do nothing  Russo One
			// $record->information = str_replace('<', '<', $record->information);
			// $record->information = str_replace('>', '>', $record->information);

			// $record->snippet_headline = str_replace('<', '<', $record->snippet_headline);
			// $record->snippet_headline = str_replace('>', '>', $record->snippet_headline);

			$record->information = html_entity_decode($record->information);
			$record->snippet_headline = html_entity_decode($record->snippet_headline);

		} else {
			$record->information = $this->prep_record_info($record->information);
			$record->snippet_headline = $this->prep_record_info($record->snippet_headline);
		}
		
		$date_created = date('jS F Y', $record->date_created);
		$record->date_created = 'Date Created: '.$date_created;

		if ($record->last_updated>1000) {
			$last_updated = 'Last Updated: '.date('jS F Y', $record->last_updated);
		} else {
			$last_updated = str_replace('Date Created', 'Last Updated', $record->date_created);
		}

		$record->last_updated = $last_updated;

		http_response_code(200);
		echo json_encode($record);		
	}

	function prep_record_info($information) {
		$information = nl2br($information);
		$information = str_replace('[code]', '<pre><code>', $information);
		$information = str_replace('[/code]', '</code></pre>', $information);
		$information = str_replace('</code></pre><br />', '</code></pre>', $information);
		$information = str_replace('<br />\n<pre><code>', '<pre><code>', $information);

		$information = $this->make_code_good($information);
		return $information;
	}

	function make_code_good($information) {

		$code_blocks = explode('<pre><code>', $information);
		if (isset($code_blocks[0])) {
			unset($code_blocks[0]);
		}

		$good_code_blocks = [];

		$count = 0;
		foreach ($code_blocks as $code_block) {
			$count++;

			//find out where </code> is...
			$code_close_pos = strpos($code_block, '</code>');

			if (is_numeric($code_close_pos)) {
				$code_block = substr($code_block, 0, $code_close_pos);
			}

			//build up an array of GOOD code block content
			$good_code_block = str_replace('<br>', '', $code_block);
			$good_code_block = str_replace('<br />', '', $good_code_block);
			$good_code_block = ltrim(trim($good_code_block));
			$good_code_blocks[$count] = $good_code_block;

			$ditch = $code_block;
			$replace = '<code-block-content-'.$count.'>';
			$information = str_replace($ditch, $replace, $information);

			foreach ($good_code_blocks as $key => $value) {
				$ditch = '<code-block-content-'.$key.'>';
				$replace = $value;
				$information = str_replace($ditch, $value, $information);
			}



		}

		//$information = str_replace('<pre><code>', '[code]', $information);
		//$information = str_replace('</pre></code>', '[/code]', $information);

		return $information;
	}

	function submit_record($update_id) {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		if (!is_numeric($update_id)) {
			die();
		}

		$posted_data = file_get_contents('php://input');
		$data = json_decode($posted_data);

		$errors = [];

		$data->snippet_headline = trim($data->snippet_headline);
		$data->information = trim($data->information);

		if ($data->snippet_headline == '') {
			$errors[] = 'You did not submit a record headline.';
		}

		if ($data->information == '') {
			$errors[] = 'You did not submit a record body.';
		}

		$num_errors = count($errors);

		if ($num_errors>0) {
			http_response_code(422);

			foreach ($errors as $error) {
				echo $error;
				die();
			}

		} else {
			http_response_code(200);

			$data = (array) $data;

			$ditch = '<';
			$replace = '<';
			$data['information'] = str_replace($ditch, $replace, $data['information']);

			$ditch = '>';
			$replace = '>';
			$data['information'] = str_replace($ditch, $replace, $data['information']);

			$ditch = '<';
			$replace = '<';
			$data['snippet_headline'] = str_replace($ditch, $replace, $data['snippet_headline']);

			$ditch = '>';
			$replace = '>';
			$data['snippet_headline'] = str_replace($ditch, $replace, $data['snippet_headline']);

			$data['date_created'] = time();
			$data['last_updated'] = time();
			$data['code'] = make_rand_str(32);
			$data['public'] =0;

			if (isset($data['id'])) {
				$update_id = $data['id'];
			} else {
				$update_id = 0;
			}

			if (!is_numeric($update_id)) {
				$update_id = 0;
			}

			$data['information'] = htmlentities($data['information']);

			if ($update_id>0) {
				$new_data['last_updated'] = $data['last_updated'];
				$new_data['information'] = $data['information'];
				$new_data['snippet_headline'] = $data['snippet_headline'];
				$this->model->update($update_id, $new_data, 'snippets');
			} else {
				$update_id = $this->model->insert($data, 'snippets');
			}

			echo $update_id;
		}

	}

	function ditch_category($update_id) {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		if (!is_numeric($update_id)) {
			die();
		}

		$params['category_id'] = $update_id;
		$sql = 'select * from snippets where category_id=:category_id';
		$rows = $this->model->query_bind($sql, $params, 'object');

		if (count($rows)>0) {
			$error = 'Unable to delete since at least one record is assigned to that category';
		} else {
			$sql = 'select * from snippet_categories where id=:category_id';
			$rows = $this->model->query_bind($sql, $params, 'object');

			if ($rows<1) {
				$error = 'Invalid category selection.';
			}
		}

		if (isset($error)) {
			http_response_code(422);
			echo $error;
		} else {
			$sql = 'delete from snippet_categories where id=:category_id';
			$rows = $this->model->query_bind($sql, $params, 'object');
			http_response_code(200);
		}
	}

	function submit_category() {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		$posted_data = file_get_contents('php://input');
		$data = json_decode($posted_data);
		$data = (array) $data;

		$errors = [];

		$category_data['category_title'] = ltrim(trim($data['category_title']));
		$update_id = $data['update_id'];

		if (!is_numeric($update_id)) {
			$error = 'Non-numeric category ID!';
		} elseif($category_data['category_title'] == '') {
			$error = 'You did not submit a valid category title';
		}

		if (isset($error)) {
			http_response_code(422);
			echo $error;
		} else {

			if ($update_id>0) {
				$this->model->update($update_id, $category_data, 'snippet_categories');
			} else {
				$category_data['user_id'] = 1;
				$category_data['code'] = make_rand_str(32);
				$update_id = $this->model->insert($category_data, 'snippet_categories');
			}

			http_response_code(200);
			echo $update_id;

		}
		
	}

	function ditch_record($update_id) {
		header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");

		if (!is_numeric($update_id)) {
			die();
		}

		$params['id'] = $update_id;
		$sql = 'delete from snippets where id =:id';
		$this->model->query_bind($sql, $params);

		http_response_code(200);
		echo 'done';

		//http_response_code(422);
		//echo 'Some error vibe';


	}

	function _interupt($data) {
	    header("Content-type:application/json");
		header("Access-Control-Allow-Origin: *");
		$html_code = $this->view('test_interupt', $data, true);
		http_response_code(203);
		echo $html_code; die();
	}
}