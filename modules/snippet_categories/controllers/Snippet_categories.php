<?php
class Snippet_categories extends Trongate {

    function manage() {
        $this->module('security');
        $data['token'] = $this->security->_make_sure_allowed();
        $data['order_by'] = 'id';

        //format the pagination
        $data['total_rows'] = $this->model->count('snippet_categories');
        $data['record_name_plural'] = 'snippet categories';

        $data['headline'] = 'Manage Snippet Categories';
        $data['view_module'] = 'snippet_categories';
        $data['view_file'] = 'manage';

        $this->template('admin', $data);
    }

    function show() {
        $this->module('security');
        $token = $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('snippet_categories/manage');
        }

        $data = $this->_get_data_from_db($update_id);
        $data['token'] = $token;

        if ($data == false) {
            redirect('snippet_categories/manage');
        } else {
            $data['form_location'] = BASE_URL.'snippet_categories/submit/'.$update_id;
            $data['update_id'] = $update_id;
            $data['headline'] = 'Snippet Category Information';
            $data['view_file'] = 'show';
            $this->template('admin', $data);
        }
    }

    function create() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);
        $submit = $this->input('submit', true);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('snippet_categories/manage');
        }

        //fetch the form data
        if (($submit == '') && ($update_id > 0)) {
            $data = $this->_get_data_from_db($update_id);
        } else {
            $data = $this->_get_data_from_post();
        }

        $data['headline'] = $this->_get_page_headline($update_id);

        if ($update_id > 0) {
            $data['cancel_url'] = BASE_URL.'snippet_categories/show/'.$update_id;
            $data['btn_text'] = 'UPDATE SNIPPET CATEGORY DETAILS';
        } else {
            $data['cancel_url'] = BASE_URL.'snippet_categories/manage';
            $data['btn_text'] = 'CREATE SNIPPET CATEGORY RECORD';
        }

        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css';
        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/i18n/jquery-ui-timepicker-addon-i18n.min.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/jquery-ui-sliderAccess.js';
        $data['additional_includes_top'] = $additional_includes_top;

        $data['form_location'] = BASE_URL.'snippet_categories/submit/'.$update_id;
        $data['update_id'] = $update_id;
        $data['view_file'] = 'create';
        $this->template('admin', $data);
    }

    function _get_page_headline($update_id) {
        //figure out what the page headline should be (on the snippet_categories/create page)
        if (!is_numeric($update_id)) {
            $headline = 'Create New Snippet Category Record';
        } else {
            $headline = 'Update Snippet Category Details';
        }

        return $headline;
    }

    function submit() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {

            $this->validation_helper->set_rules('user_id', 'User ID', 'required|max_length[11]|numeric|greater_than[0]|integer');
            $this->validation_helper->set_rules('category_title', 'Category Title', 'required|min_length[2]|max_length[255]');
            $this->validation_helper->set_rules('code', 'Code', 'required|min_length[2]|max_length[255]');

            $result = $this->validation_helper->run();

            if ($result == true) {

                $update_id = $this->url->segment(3);
                $data = $this->_get_data_from_post();
                if (is_numeric($update_id)) {
                    //update an existing record
                    $this->model->update($update_id, $data, 'snippet_categories');
                    $flash_msg = 'The record was successfully updated';
                } else {
                    //insert the new record
                    $update_id = $this->model->insert($data, 'snippet_categories');
                    $flash_msg = 'The record was successfully created';
                }

                set_flashdata($flash_msg);
                redirect('snippet_categories/show/'.$update_id);

            } else {
                //form submission error
                $this->create();
            }

        }

    }

    function submit_delete() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {
            $update_id = $this->url->segment(3);

            if (!is_numeric($update_id)) {
                die();
            } else {
                $data['update_id'] = $update_id;

                //delete all of the comments associated with this record
                $sql = 'delete from comments where target_table = :module and update_id = :update_id';
                $data['module'] = $this->module;
                $this->model->query_bind($sql, $data);

                //delete the record
                $this->model->delete($update_id, $this->module);

                //set the flashdata
                $flash_msg = 'The record was successfully deleted';
                set_flashdata($flash_msg);

                //redirect to the manage page
                redirect('snippet_categories/manage');
            }
        }
    }

    function _get_data_from_db($update_id) {
        $snippet_categories = $this->model->get_where($update_id, 'snippet_categories');

        if ($snippet_categories == false) {
            $this->template('error_404');
            die();
        } else {
            $data['user_id'] = $snippet_categories->user_id;
            $data['category_title'] = $snippet_categories->category_title;
            $data['code'] = $snippet_categories->code;
            return $data;
        }
    }

    function _get_data_from_post() {
        $data['user_id'] = $this->input('user_id', true);
        $data['category_title'] = $this->input('category_title', true);
        $data['code'] = $this->input('code', true);
        return $data;
    }

}