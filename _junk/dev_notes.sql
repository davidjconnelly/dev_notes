-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 29, 2020 at 09:34 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_notes`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `date_created` int(11) DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `target_table` varchar(125) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `code` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `licence_keys`
--

CREATE TABLE `licence_keys` (
  `id` int(11) NOT NULL,
  `licence_key` varchar(32) NOT NULL,
  `date_created` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `licence_keys`
--

INSERT INTO `licence_keys` (`id`, `licence_key`, `date_created`, `transaction_id`) VALUES
(1, 'abc123', 1598727163, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `snippets`
--

CREATE TABLE `snippets` (
  `id` int(11) NOT NULL,
  `snippet_headline` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `last_updated` int(11) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `information` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `snippets`
--

INSERT INTO `snippets` (`id`, `snippet_headline`, `category_id`, `date_created`, `code`, `last_updated`, `public`, `information`) VALUES
(1, 'How to do an IF statement', 1, 1598689360, '4UdLZAW29DbZ27823Cjr9R6YEG6JNPw9', 1598689387, 0, 'Here we go:\n\n[code]\n&lt;?php \nif ($something == \'whatever\') {\n    echo \'hello\';\n}\n?&gt;\n[/code]\n\nI hope this works!'),
(3, 'How to say \'hello\' UPdated!!!', 1, 1598690835, 'DZ8Uhj5VfMGeZfReDDVgEjtaenAD87cP', 1598698317, 0, 'Here we go:\n\n[code]\n&lt;?= \'hello\' ?&gt;\n\n&lt;script&gt;\nalert(&quot;hello world&quot;);\n&lt;/script&gt;\n\n[/code]\n\nRock and roll!'),
(4, 'How To Do An Alert', 2, 1598692776, 'fYfmv97RqMN4dKbrVAmaqKzjkS9GSUHV', 1598692807, 0, 'You can do an alert like so:\n\n[code]\n&lt;script&gt;\n    alert(&quot;hello world&quot;);\n&lt;/script&gt;\n[/code]\n\nI hope this works!'),
(5, 'How to do something', 1, 1598695722, 'K7Q8kDwP7gE37jFC7hDaDAHgRk7hf5gD', 1598695722, 0, 'Here is an unusual word for you.  How about lamppost?'),
(6, 'I want to ride my bicycle', 1, 1598695826, '6TK2Y9YW5gSTXZ38nByff8nZ5kwmsSyJ', 1598695826, 0, 'here we go again.'),
(7, 'New PHP Record Updated', 1, 1598698106, 'fhZLj7u7RSQ8p7yU7duc2X62D7PbUvwr', 1598698140, 0, 'Here we go');

-- --------------------------------------------------------

--
-- Table structure for table `snippet_categories`
--

CREATE TABLE `snippet_categories` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_title` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `snippet_categories`
--

INSERT INTO `snippet_categories` (`id`, `user_id`, `category_title`, `code`) VALUES
(1, 1, 'PHP', 'AXh9pg5uVMtQNEcNxAN2dmud5amGLzWR'),
(2, 1, 'JavaScript', 'fwPpxhL3R9pSCYXEFJPsGHy8BgmaNDPL'),
(3, 1, 'C++', 'yBjQMPpS457ApxvVXb6GGZuE2jjf8Zvt');

-- --------------------------------------------------------

--
-- Table structure for table `trongate_administrators`
--

CREATE TABLE `trongate_administrators` (
  `id` int(11) NOT NULL,
  `username` varchar(65) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `trongate_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_administrators`
--

INSERT INTO `trongate_administrators` (`id`, `username`, `password`, `trongate_user_id`) VALUES
(1, 'admin', '$2y$11$SoHZDvbfLSRHAi3WiKIBiu.tAoi/GCBBO4HRxVX1I3qQkq3wCWfXi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trongate_tokens`
--

CREATE TABLE `trongate_tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(125) DEFAULT NULL,
  `user_id` int(11) DEFAULT 0,
  `expiry_date` int(11) DEFAULT NULL,
  `code` varchar(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_tokens`
--

INSERT INTO `trongate_tokens` (`id`, `token`, `user_id`, `expiry_date`, `code`) VALUES
(1, '4F_EIS19nVQrYg-B693aZtR6QSfz9REx', 1, 1598775205, '0'),
(4, 'rDA72lnu7YMTzIyrceVozi3M1rAGh6hI', 0, 1598704797, 'aaa'),
(5, 'jY01daiRHDPKi9OnoLlZfCx3q5TGbBCa', 1, 1598732143, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `trongate_users`
--

CREATE TABLE `trongate_users` (
  `id` int(11) NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `user_level_id` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_users`
--

INSERT INTO `trongate_users` (`id`, `code`, `user_level_id`) VALUES
(1, 'hgglXenyGOWWBYBJOGeUSM9Vg2QcoJWD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trongate_user_levels`
--

CREATE TABLE `trongate_user_levels` (
  `id` int(11) NOT NULL,
  `level_title` varchar(125) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_user_levels`
--

INSERT INTO `trongate_user_levels` (`id`, `level_title`) VALUES
(1, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licence_keys`
--
ALTER TABLE `licence_keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `snippets`
--
ALTER TABLE `snippets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `snippet_categories`
--
ALTER TABLE `snippet_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_administrators`
--
ALTER TABLE `trongate_administrators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_tokens`
--
ALTER TABLE `trongate_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_users`
--
ALTER TABLE `trongate_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_user_levels`
--
ALTER TABLE `trongate_user_levels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licence_keys`
--
ALTER TABLE `licence_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `snippets`
--
ALTER TABLE `snippets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `snippet_categories`
--
ALTER TABLE `snippet_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trongate_administrators`
--
ALTER TABLE `trongate_administrators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trongate_tokens`
--
ALTER TABLE `trongate_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trongate_users`
--
ALTER TABLE `trongate_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trongate_user_levels`
--
ALTER TABLE `trongate_user_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
